/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javainterfaces;

/**
 *
 * @author human
 */
public class Late extends Coffee {

    private Milk milk;
    private Cream cream;

    public Late(Milk milk, Cream cream, int coffeeVolume) {
        super(coffeeVolume + milk.getVolume() + cream.getVolume());
        this.milk = milk;
        this.cream = cream;
    }

    public Milk getMilk() {
        return milk;
    }

    public Cream getCream() {
        return cream;
    }

    @Override
    public String toString() {
        return "Late{" + "milk=" + milk + ", cream=" + cream + '}';
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javajdbc_hometask2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 *
 * @author Яна
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        /* getConnection();*/
        createTable();

    }

    public static void createTable() throws Exception {
        try {
            Connection conn = getConnection();
            PreparedStatement create = conn.prepareStatement("CREATE TABLE IF NOT EXISTS study.student2 ( `id` INT NOT NULL AUTO_INCREMENT, `name` CHAR(30) NOT NULL,`second_name` CHAR(30) NOT NULL, `admission_year` date , `age` SMALLINT(6) NOT NULL, PRIMARY KEY(`id`, `admission_year`) )");
            create.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            System.out.println("Table created.");
        };
    }
    
    
        public void updateTable() throws Exception {
        
        try {
            Connection conn1 = getConnection();
            PreparedStatement update = conn1.prepareStatement(
                    "UPDATE student2 SET " +
                    "id=10, name=Anna, second_name=Happy, " +
                    "admission_year=2015, age=19" +
                    "WHERE id=?");
       
        } catch (Exception e) {
            System.out.println(e);
        }finally {
            System.out.println("Table updated.");
        };
        
    }
        
          public void dropTable() throws Exception {
        
        try {
            Connection conn2 = getConnection();
            PreparedStatement drop = conn2.prepareStatement(
                    "DROP TABLE student2");
       
        } catch (Exception e) {
            System.out.println(e);
        }finally {
            System.out.println("Table dropped.");
        };
        
    }
    

    public static Connection getConnection() throws Exception {
        try {
            String driver = "com.mysql.jdbc.Driver";
            String url = "jdbc:mysql://localhost:3306/MySQL";
            String username = "root";
            String password = "root";
            Class.forName(driver);

            Connection conn = DriverManager.getConnection(url, username, password);
            System.out.println("Connected");
            return conn;
        } catch (Exception e) {
            System.out.println(e);
        }

        return null;
    }

}

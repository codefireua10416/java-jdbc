/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javajdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Class.forName(com.mysql.jdbc.Driver.class.getName());
        } catch (ClassNotFoundException ex) {
        }

        String hostname = "127.0.0.1:3306";
        String database = "test";
        String username = "root";
        String password = "codefire";

        try {
            String connectionString = String.format("jdbc:mysql://%s/%s?useSSL=false", hostname, database);
            
            System.out.println("TRYING TO CONNECT");
            Connection connection = DriverManager.getConnection(connectionString, username, password);
            System.out.println("CONNECTED");
            
            Statement stmt = connection.createStatement();
            
            // (DDL & DML)
            if (stmt.execute("SHOW DATABASES")) {
                
                ResultSet rs = stmt.getResultSet();
                
                while (rs.next()) {
                    String databaseName = rs.getString(1);
                    
                    System.out.println(databaseName);
                }
            }
            
            stmt.close();
            
            connection.close();
            System.out.println("DISCONNECTED");
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}

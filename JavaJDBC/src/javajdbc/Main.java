/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javajdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Java Database Connectivity (JDBC)

        // Load JDBC (MySQL) Driver class to memory.
        try {
//            Class.forName("com.mysql.jdbc.Driver");
            Class.forName(com.mysql.jdbc.Driver.class.getName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

//        String database  = "192.168.1.99:3306/test";
        String hostname = "localhost:3306";
        String database = "test"; // "127.0.0.1:3306/test"
        String username = "student";
        String password = "12345";

        try {
            String connectionString = String.format("jdbc:mysql://%s/%s?useSSL=false", hostname, database);
            System.out.println("TRYING TO CONNECT");
            
            Connection connection = DriverManager.getConnection(connectionString, username, password);
            
            System.out.println("CONNECTED");
            
            connection.close();
            
            System.out.println("DISCONNECTED");
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}

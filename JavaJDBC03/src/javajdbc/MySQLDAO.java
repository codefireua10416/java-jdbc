/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javajdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Data Access Object (DAO)
 *
 * @author human
 */
public class MySQLDAO {

    private Properties properties;

    public MySQLDAO(String hostname, String database, String username, String password) {
        this.properties = new Properties();
        this.properties.put("hostname", hostname);
        this.properties.put("database", database);
        this.properties.put("username", username);
        this.properties.put("password", password);
    }

    public MySQLDAO(Properties properties) {
        this.properties = properties;
    }

    public Connection getConnection() throws SQLException {
        String connectionString = String.format("jdbc:mysql://%s/%s?useSSL=false",
                properties.getProperty("hostname"), properties.getProperty("database"));

        return DriverManager.getConnection(connectionString,
                properties.getProperty("username"), properties.getProperty("password"));
    }

    public List<String> getDatabaseList() throws SQLException {
        List<String> databaseList = new ArrayList<>();

        try (Connection conn = getConnection()) {
            ResultSet rs = conn.createStatement().executeQuery("SHOW DATABASES");

            while (rs.next()) {
                databaseList.add(rs.getString(1)); // where clause
            }
        }

        return databaseList;
    }

}
